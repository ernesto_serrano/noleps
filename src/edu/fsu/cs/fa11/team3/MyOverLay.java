package edu.fsu.cs.fa11.team3;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;

public class MyOverLay extends Overlay {
    private GeoPoint gp1; // first stop
    private GeoPoint gp2;  // second stop
    private int mRadius = 6;
    private int mode = 0; // beginning, path, end
    private int defaultColor;

    public Boolean first = false;

    // GeoPoint is a integer. (6E)
    public MyOverLay(GeoPoint gp1,GeoPoint gp2,int mode) {
        this.gp1 = gp1;
        this.gp2 = gp2;
        this.mode = mode;
        defaultColor = 999;// no defaultColor
    }

    public MyOverLay(GeoPoint gp1,GeoPoint gp2,int mode, boolean first) {
        this.gp1 = gp1;
        this.gp2 = gp2;
        this.mode = mode;
        defaultColor = 999;// no defaultColor
        this.first = first; // first stop
    }

    public MyOverLay(GeoPoint gp1,GeoPoint gp2,int mode, int defaultColor) {
        this.gp1 = gp1;
        this.gp2 = gp2;
        this.mode = mode;
        this.defaultColor = defaultColor;
    }

    public int getMode() {
        return mode;
    }

    @Override
    public boolean draw
    (Canvas canvas, MapView mapView, boolean shadow, long when) {
        Projection projection = mapView.getProjection();
        if (shadow == false) {
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            Point point = new Point();
            projection.toPixels(gp1, point);
            // mode = 1 - start
            if (mode == 1) {
                if(defaultColor == 999) {
                    paint.setColor(Color.GREEN);
                } else {
                    paint.setColor(defaultColor);
                }
                // Draws the start point once
                if (first == true) {
                    paint.setColor(Color.GREEN);
                    RectF oval = new RectF(point.x - mRadius, point.y - mRadius,
                            point.x + mRadius, point.y + mRadius);
                    canvas.drawOval(oval, paint);
                }

            } else if (mode == 2) {
                // mode = 2 - path
                if(defaultColor == 999) {
                    paint.setColor(Color.BLUE);
                } else {
                    paint.setColor(defaultColor);
                }
                // Draws path
                Point point2 = new Point();
                projection.toPixels(gp2, point2);
                paint.setStrokeWidth(5);
                paint.setAlpha(120);
                canvas.drawLine(point.x, point.y, point2.x,point2.y, paint);
            } else if (mode == 3) {
                //mode=3 - end
                // the last path
                if(defaultColor == 999) {
                    paint.setColor(Color.RED);
                } else {
                    paint.setColor(defaultColor);
                }
                // Draws end points for every stop
                Point point2 = new Point();
                projection.toPixels(gp2, point2);
                RectF oval=new RectF(point2.x - mRadius,point2.y - mRadius,
                        point2.x + mRadius,point2.y + mRadius);
                paint.setAlpha(250);
                canvas.drawOval(oval, paint);
                paint.setStrokeWidth(5);
                paint.setAlpha(120);
                canvas.drawLine(point.x, point.y, point2.x,point2.y, paint);
            }// end else if mode 3
        }
        return super.draw(canvas, mapView, false, when);
    }
}