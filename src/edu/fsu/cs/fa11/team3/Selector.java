package edu.fsu.cs.fa11.team3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Selector extends Activity implements TextWatcher, LocationListener{

    private AutoCompleteTextView edit;
    private ListView routeInfo;
    private dataBase db = null;
    private Cursor constantsCursor = null;
    private double lat;
    private double lon;
    private String Building;
    private ArrayList<Bundle> routeStops = new ArrayList<Bundle>();
    private ArrayList<Bundle> singleStop = new ArrayList<Bundle>();
    LocationManager lm;
    double currentLocLat;
    double currentLocLon;
    String[] items;

    // variables for menu options
    public static final int MENU_REMOVE = Menu.FIRST+1;
    public static final int MENU_CLEAR = Menu.FIRST+2;
    public static final int MENU_QUIT = Menu.FIRST+3;
    public static final int MENU_SAVE = Menu.FIRST+4;
    public static final int MENU_LOAD = Menu.FIRST+5;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selector);

        // code for auto complete and to populate spinner
        db = new dataBase(this);
        int cIndex = 0;
        constantsCursor = db
                .getReadableDatabase()
                .rawQuery("SELECT title FROM constants", null);
        constantsCursor.moveToFirst();
        items = new String[constantsCursor.getCount()];

        if (constantsCursor.moveToFirst()) {
            for (int i = 0; i < constantsCursor.getCount(); i++) {
                items[i] = constantsCursor.getString(cIndex);
                constantsCursor.moveToNext();
            }
        }

        edit=(AutoCompleteTextView)findViewById(R.id.edit);
        edit.addTextChangedListener(this);
        edit.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, items));
        setUpStopsList();

    }

    @Override
    public void onResume() {
        // set last known location
        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000L, 500.0f, this);

        try{
            Location lastKnownLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            currentLocLat = lastKnownLocation.getLatitude();
            currentLocLon = lastKnownLocation.getLongitude();
        } catch(Exception e) {
            //continue
        }
        super.onResume();
    }

    @Override
    public void onPause()
    {
        lm.removeUpdates(this);
        super.onPause();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
            int after) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // TODO Auto-generated method stub
    }

    @Override
    public void afterTextChanged(Editable s) {
        // TODO Auto-generated method stub
    }

    public void makeRoute(View v) {
        if (routeStops.size() > 1) {
            Intent i = new Intent("edu.fsu.cs.fa11.team3.routeIntent");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putParcelableArrayListExtra("routes", routeStops);
            this.startActivity(i);
        }
        else if (routeStops.size() == 1){
            singleStop.clear();
            Bundle single = new Bundle();
            single.putString("Name", "Current Location");
            single.putDouble("lat", currentLocLat);
            single.putDouble("lon", currentLocLon);
            singleStop.add(single);
            singleStop. add(routeStops.get(0));
            Intent i = new Intent("edu.fsu.cs.fa11.team3.routeIntent");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putParcelableArrayListExtra("routes", singleStop);
            this.startActivity(i);
        }
    }

    public void addStop(View v) {
        Building = edit.getText().toString();
        Bundle stopToAdd = new Bundle();

        if (Arrays.asList(items).contains(Building)) {
            //*******Testing DataBase
            constantsCursor=db
                    .getReadableDatabase()
                    .rawQuery("SELECT lat, lon FROM constants WHERE title = '"+Building+"'", null);
            constantsCursor.moveToFirst();
            lat = constantsCursor.getDouble(0);
            lon = constantsCursor.getDouble(1);
            String theLat = lat+"";
            Log.w("got:", theLat);
            String theLon = lon+"";
            Log.w("got:", theLon);
            //*******Testing Database

            stopToAdd.putString("Name", Building);

            if (Building.equals("Current Location")) {
                stopToAdd.putDouble("lat", currentLocLat);
                stopToAdd.putDouble("lon", currentLocLon);
            } else {
                stopToAdd.putDouble("lat", lat);
                stopToAdd.putDouble("lon", lon);
            }

            routeStops.add(stopToAdd);
            ((BaseAdapter) routeInfo.getAdapter()).notifyDataSetChanged();
            edit.setText("");
        }
    }

    private void setUpStopsList() {
        routeInfo = (ListView) findViewById(R.id.routeInfo);
        registerForContextMenu(routeInfo);
        RouteAdapter routeAdapter = new RouteAdapter();
        routeInfo.setAdapter(routeAdapter);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
            ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(Menu.NONE, MENU_REMOVE, Menu.NONE, "Remove");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        ArrayAdapter<Bundle> adapter = new RouteAdapter();

        switch (item.getItemId()) {
            case MENU_REMOVE:
                adapter.remove(routeStops.get(info.position));
                ((BaseAdapter) routeInfo.getAdapter()).notifyDataSetChanged();
                return(true);
        }
        return(super.onContextItemSelected(item));
    }

    public class RouteAdapter extends ArrayAdapter<Bundle> {

        RouteAdapter() {
            super(Selector.this,R.layout.stop,routeStops);
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            View stop = convertView;
            TextView textview;
            LayoutInflater inflater = getLayoutInflater();
            stop = inflater.inflate(R.layout.stop, parent, false);
            textview = (TextView) stop.findViewById(R.id.stop);
            textview.setText(routeStops.get(pos).getString("Name"));
            return stop;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocLat = location.getLatitude();
        currentLocLon = location.getLongitude();
    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu
        .add(Menu.NONE, MENU_CLEAR,Menu.NONE, "Clear Route")
        .setIcon(android.R.drawable.ic_menu_delete);
        menu
        .add(Menu.NONE, MENU_SAVE,Menu.NONE, "Save Route")
        .setIcon(android.R.drawable.ic_menu_save);
        menu
        .add(Menu.NONE, MENU_LOAD, Menu.NONE, "Load Route")
        .setIcon(android.R.drawable.ic_menu_upload);
        menu
        .add(Menu.NONE, MENU_QUIT,Menu.NONE, "Quit")
        .setIcon(android.R.drawable.ic_menu_close_clear_cancel);

        return (super.onCreateOptionsMenu(menu));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_CLEAR:
                menuClear();
                return(true);
            case MENU_SAVE:
                MenuSave();
                return(true);
            case MENU_LOAD:
                MenuLoad();
                return(true);
            case MENU_QUIT:
                menuQuit();
                return(true);
        }
        return(super.onOptionsItemSelected(item));
    }

    public void menuClear() {
        // clears current route
        routeStops.clear();
        ((BaseAdapter) routeInfo.getAdapter()).notifyDataSetChanged();
    }

    public void menuQuit() {
        // quits application
        finish();
    }

    public void MenuSave() {
        // saves current route to be loaded later
        // The Bundle object is not serializable so the information needs to be saved the long way,
        // one by one.
        String filename = "SavedRoute";
        if (routeStops.isEmpty()) {
            Toast.makeText(this, "Please add at least one stop to the route", Toast.LENGTH_SHORT)
            .show();
            return;
        }

        try {
            FileOutputStream fos = openFileOutput(filename, MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeInt(routeStops.size());
            for (Bundle b: routeStops) {
                oos.writeUTF(b.getString("Name"));
                oos.writeDouble(b.getDouble("lat"));
                oos.writeDouble(b.getDouble("lon"));
            }
            oos.flush();
            oos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Toast.makeText(this, "Saved Route", Toast.LENGTH_SHORT).show();
    }

    public void MenuLoad() {
        // loads route that is currently saved
        try {
            FileInputStream fis = openFileInput("SavedRoute");
            ObjectInputStream ois = new ObjectInputStream(fis);
            routeStops.clear();
            int elements = ois.readInt();
            for (int i = 0; i < elements; ++i) {
                Bundle b = new Bundle();
                b.putString("Name", ois.readUTF());
                b.putDouble("lat", ois.readDouble());
                b.putDouble("lon", ois.readDouble());
                routeStops.add(b);
            }
            ((BaseAdapter) routeInfo.getAdapter()).notifyDataSetChanged();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Toast.makeText(this, "Loaded Saved Route", Toast.LENGTH_SHORT).show();
    }
}