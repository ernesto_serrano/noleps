package edu.fsu.cs.fa11.team3;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class RouteMapActivity extends MapActivity {
    /* Called when the activity is first created. */

    MapView mapView;
    List<Overlay> mapOverlays;
    Drawable drawable;
    Drawable[] drawableArray = new Drawable[10];
    int stopCounter = 0;
    CustomItemizedOverlay<CustomOverlayItem> mItemizedOverlay;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        GeoPoint begin = new GeoPoint(0, 0);
        Bundle beg = new Bundle();
        Bundle end = new Bundle();


        drawableArray[0] = this.getResources().getDrawable(R.drawable.a);
        drawableArray[1] = this.getResources().getDrawable(R.drawable.b);
        drawableArray[2] = this.getResources().getDrawable(R.drawable.c);
        drawableArray[3] = this.getResources().getDrawable(R.drawable.d);
        drawableArray[4] = this.getResources().getDrawable(R.drawable.e);
        drawableArray[5] = this.getResources().getDrawable(R.drawable.f);
        drawableArray[6] = this.getResources().getDrawable(R.drawable.g);
        drawableArray[7] = this.getResources().getDrawable(R.drawable.h);
        drawableArray[8] = this.getResources().getDrawable(R.drawable.i);
        drawableArray[9] = this.getResources().getDrawable(R.drawable.j);

        MapView mapView = (MapView) findViewById(R.id.myMapView1);
        mapView.setBuiltInZoomControls(true);


        Intent i = getIntent();
        ArrayList<Parcelable> routes = i.getParcelableArrayListExtra("routes");

        beg = (Bundle)routes.get(0);
        for (int j = 1; j < routes.size(); j++){
            end = (Bundle)routes.get(j);

            if (j == 1) {
                DrawPath(beg,end,999,mapView,true);

                double srcLat = beg.getDouble("lat");
                double srcLong = beg.getDouble("lon");
                GeoPoint srcGeoPoint = new GeoPoint((int) (srcLat * 1E6), (int) (srcLong * 1E6));
                begin = srcGeoPoint;
            } else {
                DrawPath(beg,end,999,mapView,false);
            }
            beg = end;
        }
        mapView.getController().animateTo(begin);
        mapView.getController().setZoom(19);
    }

    @Override
    protected boolean isRouteDisplayed() {
        // TODO Auto-generated method stub
        return false;
    }

    //private void DrawPath(GeoPoint src,GeoPoint dest, int color, MapView mapView, boolean first)
    private void DrawPath(Bundle beg,Bundle end, int color, MapView mapView, boolean first)
    {
        double srcLat = beg.getDouble("lat");
        double srcLong = beg.getDouble("lon");
        double destLat = end.getDouble("lat");
        double destLong = end.getDouble("lon");

        GeoPoint src = new GeoPoint((int) (srcLat * 1E6), (int) (srcLong * 1E6));
        GeoPoint dest = new GeoPoint((int) (destLat * 1E6), (int) (destLong * 1E6));

        // connect to map web service
        StringBuilder urlString = new StringBuilder();
        urlString.append("http://maps.google.com/maps?f=d&hl=en");
        urlString.append("&saddr=");//from
        urlString.append( Double.toString(src.getLatitudeE6()/1.0E6 ));
        urlString.append(",");
        urlString.append( Double.toString(src.getLongitudeE6()/1.0E6 ));
        urlString.append("&daddr=");//to
        urlString.append( Double.toString(dest.getLatitudeE6()/1.0E6 ));
        urlString.append(",");
        urlString.append( Double.toString(dest.getLongitudeE6()/1.0E6 ));
        urlString.append("&dirflg=w&hl=en&ie=UTF8&z=14&output=kml");
        Log.d("xxx","URL="+urlString.toString());
        // get the kml (XML) doc. And parse it to get the coordinates(direction route).
        Document doc = null;
        HttpURLConnection urlConnection= null;
        URL url = null;
        try {
            url = new URL(urlString.toString());
            urlConnection=(HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.connect();

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(urlConnection.getInputStream());

            if (doc.getElementsByTagName("GeometryCollection").getLength()>0) {
                String path = doc.getElementsByTagName("GeometryCollection").item(0).
                        getFirstChild().getFirstChild().getFirstChild().getNodeValue() ;
                Log.d("xxx","path="+ path);
                String [] pairs = path.split(" ");
                String[] lngLat = pairs[0].split(","); // lngLat[0]=longitude lngLat[1]=latitude lngLat[2]=height
                // Source
                GeoPoint startGP = new GeoPoint((int)(Double.parseDouble(lngLat[1])*1E6),
                        (int)(Double.parseDouble(lngLat[0])*1E6));
                String coords = lngLat[1] + "," + lngLat[0];
                String theURL = getResources().getString(R.string.mapURL);
                String streetImageUrl = String.format(theURL, coords, coords);

                if(first == true) {
                    mapOverlays = mapView.getOverlays();
                    drawable = drawableArray[stopCounter];
                    mItemizedOverlay = new CustomItemizedOverlay<CustomOverlayItem>(drawable,
                            mapView);
                    stopCounter++;
                    /*OverlayItem overlayItem = new OverlayItem(startGP, beg.getString("Name"), "");
                    stopOverlay.addOverlay(overlayItem);
                     */
                    Log.w("ok", streetImageUrl);
                    CustomOverlayItem overlayItem = new CustomOverlayItem(startGP, beg.getString("Name"),
                            " ",streetImageUrl);
                    mItemizedOverlay.addOverlay(overlayItem);
                    mapOverlays.add(mItemizedOverlay);

                    //mapView.getOverlays().add(new MyOverLay(startGP, startGP, 1, true));
                } else {
                    mapView.getOverlays().add(new MyOverLay(startGP, startGP, 1, false));
                }


                GeoPoint gp1;
                GeoPoint gp2 = startGP;

                // the last one would be crash
                for(int i = 1; i < pairs.length; i++) {
                    lngLat = pairs[i].split(",");
                    gp1 = gp2;
                    // watch out! For GeoPoint, first:latitude, second:longitude
                    gp2 = new GeoPoint((int)(Double.parseDouble(lngLat[1])*1E6),(int)(Double.parseDouble(lngLat[0])*1E6));
                    mapView.getOverlays().add(new MyOverLay(gp1,gp2,2,color));
                    Log.d("xxx","pair:" + pairs[i]);
                }

                coords = lngLat[1] + "," + lngLat[0];
                theURL = getResources().getString(R.string.mapURL);
                streetImageUrl = String.format(theURL, coords, coords);
                mapOverlays = mapView.getOverlays();
                drawable = drawableArray[stopCounter];
                //OverlayItem overlayItem = new OverlayItem(dest, end.getString("Name"), "");
                //stopOverlay.addOverlay(overlayItem);
                mItemizedOverlay = new CustomItemizedOverlay<CustomOverlayItem>(drawable,
                        mapView);
                CustomOverlayItem overlayItem = new CustomOverlayItem(dest, end.getString("Name"),
                        " ",streetImageUrl);
                mItemizedOverlay.addOverlay(overlayItem);
                mapOverlays.add(mItemizedOverlay);
                stopCounter++;

                //mapView.getOverlays().add(new MyOverLay(dest,dest, 3));// use the default color
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }
}